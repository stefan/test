#include <iostream>

#include "Test.h"

int main(int argc, char *argv[]){
  std::cout << "starting main" << std::endl;
  Test test();
  std::cout << "exiting main" << std::endl;
  return 0;
}
