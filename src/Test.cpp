#include <iostream>
#include "Test.h"

Test::Test()
  : testVar(0)
{
  std::cout << "Test ctor" << std::endl;
}

Test::~Test()
{
  std::cout << "Test dtor" << std::endl;
}
